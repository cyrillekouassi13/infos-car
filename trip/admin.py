from django.contrib import admin
from .models import Country, Company, Trip
# Register your models here.

admin.site.register(Country)
admin.site.register(Company)
admin.site.register(Trip)