from django.forms import ModelForm
from django import forms
from .models import Trip 

PRICES_CHOICES =(
    ("bas", "Prix bas"),
    ("haut", "Prix haut"),
)

class TripForm(ModelForm):
    prix = forms.ChoiceField(choices ="PRICES_CHOICES")
    class Meta:
        model = Trip
        fields = '__all__'
