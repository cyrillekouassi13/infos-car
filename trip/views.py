from django.views.generic.list import ListView
from .filters import TripFilter
from .forms import TripForm 
from .models import Trip
from django.shortcuts import render
from django.core.paginator import Paginator



# Create your views here.

def page_not_found_view(request, exception):
    return render(request, '404.html', status=404)

class TripListView(ListView):
    model = Trip
    context_object_name = 'trips'
    template_name = 'trip/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = TripForm(self.request.GET)
        context['filter'] = TripFilter(self.request.GET, queryset=self.get_queryset())
        
        return context