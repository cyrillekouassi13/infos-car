import django_filters
from .models import Trip

class TripFilter(django_filters.FilterSet):
    CHOICES = (
        ('haut', 'Prix haut'),
        ('bas', 'Prix bas')
    )
    prix= django_filters.ChoiceFilter(label='Prix', choices=CHOICES, method='filter_by_order')

    class Meta:
        model = Trip
        fields = {
            'company': ['exact'],
            'start' : ['exact'],
            'arrival' : ['exact'],
        }
    
    def filter_by_order(self, queryset, name, value):
        expression = 'price' if value == 'bas' else '-price'
        return queryset.order_by(expression)