# Generated by Django 3.2.9 on 2023-01-10 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0006_alter_company_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.FileField(default='company-logo/company.png', upload_to='company-logo'),
        ),
    ]
