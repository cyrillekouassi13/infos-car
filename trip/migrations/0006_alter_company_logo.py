# Generated by Django 3.2.9 on 2023-01-10 16:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0005_alter_trip_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.FileField(default='company.png', upload_to='company-logo'),
        ),
    ]
