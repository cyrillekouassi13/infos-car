from django.db import models
from django.conf import settings
from django.core.files.storage import FileSystemStorage
# Create your models here.
fs = FileSystemStorage(location='/media')

class Country(models.Model):
    name = models.CharField(max_length=100)
    flags = models.FileField(upload_to='country-flags', default='settings.MEDIA_ROOT/flag.svg')

    def __str__(self):
        return self.name


class Company(models.Model):
    name = models.CharField(max_length=200)
    countries = models.ManyToManyField(Country, related_name='companies')
    logo = models.FileField(upload_to='company-logo', default='company-logo/company.png')
    description = models.CharField(max_length=255)
    website = models.URLField(max_length = 200, blank=True)

    def __str__(self):
        return self.name

class Trip(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    start = models.CharField(max_length=200)
    arrival = models.CharField(max_length=200)
    price = models.PositiveIntegerField()

    def __str__(self):
        return self.arrival
        #f'{self.company} : {self.start} --> {self.arrival} : {self.price}'